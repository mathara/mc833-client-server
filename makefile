CC=gcc
CFLAGS=
SQLITE=-l sqlite3

server: server.c
	$(CC) -o server server.c $(CFLAGS) $(SQLITE)

client: client.c
	$(CC) -o client client.c $(CFLAGS)
