#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <arpa/inet.h>

int main(int argc,char **argv)
{
  FILE *f;
  int sockfd;
  char recvline[5000];
  char sendline[5000];
  struct sockaddr_in endserver;
  /*get time */
  struct timeval tv1, tv2, deltatime, deltatimeS, conndeltatime;

  //cria um socket de um dominio(AF_INET,dominio internet) e do tipo,SOCK_STREAM
  //limpa o endereco, seta um novo
  //escuta na porta 22000
  sockfd=socket(AF_INET,SOCK_STREAM,0);
  bzero(&endserver,sizeof endserver);
  endserver.sin_family=AF_INET;
  endserver.sin_port=htons(22000);

  //Seta o endereço IP no endserver
  // o endereço em endserver deve ser no formato interio para a função inet_pton
  inet_pton(AF_INET,"143.106.16.60",&(endserver.sin_addr));

  //conecta no dispositivo cujo endereço e número da porta é especificado em endserver
  connect(sockfd,(struct sockaddr *)&endserver,sizeof(endserver));

  f = fopen("out_tc.txt", "w");
  while(1)
  {
    /*cria arquivo da respota da consulta*/
    f = fopen("out_tc.txt", "a");

    //realiza limpeza das strings de envio e recebimento
    bzero( sendline, 5000);
    bzero( recvline, 5000);
    fgets(sendline,5000,stdin); /*stdin = 0 , for standard input */

    /*time 1*/
    gettimeofday(&tv1, NULL);

    //escreve a mensagem enviada no socket
    //realiza a leitura da mensagem recebida
    write(sockfd,sendline,strlen(sendline)+1);
    read(sockfd,recvline,5000);
    printf("%s\n",recvline);
    read(sockfd,&deltatimeS,sizeof(struct timeval));
    printf("%ld.%06ld\n", deltatimeS.tv_sec, deltatimeS.tv_usec);

    /*time 2*/
    gettimeofday(&tv2, NULL);
    printf("TV1 %ld.%06ld\n", tv1.tv_sec, tv1.tv_usec);
    printf("TV2 %ld.%06ld\n", tv2.tv_sec, tv2.tv_usec);
    deltatime.tv_sec = tv2.tv_sec*1e6 - tv1.tv_sec*1e6;
    deltatime.tv_usec = tv2.tv_usec - tv1.tv_usec;
    printf("DELTA %ld.%06ld\n", deltatime.tv_sec, deltatime.tv_usec);
    fprintf(f,"%ld.%06ld\n", deltatime.tv_sec, deltatime.tv_usec);

    //conndeltatime.tv_sec = deltatime.tv_sec - deltatimeS.tv_sec;
    //conndeltatime.tv_usec = deltatime.tv_usec - deltatimeS.tv_usec;
    //printf("%ld.%06ld\n", conndeltatime.tv_sec, conndeltatime.tv_usec);
    //printf("%ld.%06ld %ld.%06ld %ld.%06ld\n",deltatimeS.tv_sec, deltatimeS.tv_usec, deltatime.tv_sec, deltatime.tv_usec, conndeltatime.tv_sec, conndeltatime.tv_usec);
    //fprintf(f,"%ld.%06ld %ld.%06ld %ld.%06ld\n",deltatimeS.tv_sec, deltatimeS.tv_usec, deltatime.tv_sec, deltatime.tv_usec, conndeltatime.tv_sec, conndeltatime.tv_usec);

    /* fecha arquivo */
    fclose(f);
  }

}
